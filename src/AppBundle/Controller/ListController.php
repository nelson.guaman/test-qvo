<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class ListController extends Controller
{



   
   /**
    * [showAction Servicios/Planes asociados a QVO]
    * @param  Request $request [description]
    * @return [type]           [description]
    */
   
    /**
    * @Route("/", name="welcome")
    */

    public function showAction(Request $request)
    {

        /* all plans from QVO */
        $client = new \GuzzleHttp\Client();

        $body = $client->request('GET', 'https://playground.qvo.cl/plans', [
          'headers' => [
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9jX0tnWkpMUXhVM1QxanJrbGZ6T0lRIiwiYXBpX3Rva2VuIjp0cnVlfQ.ZQ0VmKNcsno_SrcfVItW78KwACIkYHA-aZJkgi9LKGE'
          ]
        ])->getBody();

        $services = json_decode($body);

        if($this->getUser()){

            $email = $this->getUser()->getUsername();
            $name = $this->getUser()->getName();

            // Are we subscribed?
            foreach ($services as $key => $value) {

                $body = $client->request('GET', 'https://playground.qvo.cl/plans/' . $value->id, [
                  'headers' => [
                    'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9jX0tnWkpMUXhVM1QxanJrbGZ6T0lRIiwiYXBpX3Rva2VuIjp0cnVlfQ.ZQ0VmKNcsno_SrcfVItW78KwACIkYHA-aZJkgi9LKGE'
                  ]
                ])->getBody();

                $subs = json_decode($body);

                $services[$key]->has = false;

                foreach($subs->subscriptions as $skey => $sub){
                    if($sub->customer->email == $email){
                        $services[$key]->has = true;
                    }
                }
            }
            
        }else{
            $name = null;
        }

        return $this->render('default/index.html.twig', array('services' => $services,'name' => $name));
    }

    /**
    * @Route("/subscribe/{id}", name="subscribe")
    */
    public function toSubscribe($id)
    {

        $client = new \GuzzleHttp\Client();

        $body = $client->request('GET', 'https://playground.qvo.cl/customers', [
          'headers' => [
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9jX0tnWkpMUXhVM1QxanJrbGZ6T0lRIiwiYXBpX3Rva2VuIjp0cnVlfQ.ZQ0VmKNcsno_SrcfVItW78KwACIkYHA-aZJkgi9LKGE'
          ]
        ])->getBody();

        $response = json_decode($body);

        //list of clients - avoid to store keys in the database.
        $clients = [];
        foreach ($response as $key => $value) {
            $clients[$value->email] = $value->id;
        }

        // get the email from the current user
        $email = $this->getUser()->getUsername();
        //current ID QVO
        $tokenUser = $clients[$email];

        // subscribe
        $body = $client->request('POST', 'https://playground.qvo.cl/subscriptions', [
          'json' => [
            'customer_id' => $tokenUser,
            'plan_id' => $id
          ],
          'headers' => [
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9jX0tnWkpMUXhVM1QxanJrbGZ6T0lRIiwiYXBpX3Rva2VuIjp0cnVlfQ.ZQ0VmKNcsno_SrcfVItW78KwACIkYHA-aZJkgi9LKGE'
          ]
        ])->getBody();

        $response = json_decode($body);

        $data = ['status' => true,'data' => [],'msg' => 'Suscrito satisfactoriamente'];

        return $this->json($data);

    }

}