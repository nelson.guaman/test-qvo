<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        // Create a new blank user and process the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encode the new users password
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // Set their role
            $user->setRole('ROLE_USER');

            // Save
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            //Client QVO - register
            $client = new \GuzzleHttp\Client();

            // get all inputs from form
            $userData = $request->request->all()['user'];

            $body = $client->request('POST', 'https://playground.qvo.cl/customers', [
              'json' => [
                'email' => $userData['email'],
                'name' =>  $userData['name']
              ],
              'headers' => [
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9jX0tnWkpMUXhVM1QxanJrbGZ6T0lRIiwiYXBpX3Rva2VuIjp0cnVlfQ.ZQ0VmKNcsno_SrcfVItW78KwACIkYHA-aZJkgi9LKGE'
              ]
            ])->getBody();

            //Debug
            //$response = json_decode($body);
            //dump($response);
            //exit();

            return $this->redirectToRoute('login');
        }

        return $this->render('auth/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}